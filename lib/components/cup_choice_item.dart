import 'package:flutter/material.dart';

class CupChoiceItem extends StatefulWidget {
  const CupChoiceItem(
      {super.key, required this.pandon, required this.currentMoney});

  final num pandon; // 100
  final num currentMoney; // 0

  @override
  State<CupChoiceItem> createState() => _CupChoiceItemState();
}

class _CupChoiceItemState extends State<CupChoiceItem> {
  bool _isOpen = false;
  String imgSrc = 'assets/cup.png';

  void _calculateStart() {
    if (!_isOpen) {
      setState(() {
        _isOpen = true;
      });
    }
  }

  void _calculateImgSrc() {
    String tempImgSrc = '';
    if (_isOpen) {
      if (widget.pandon < widget.currentMoney) {
        tempImgSrc = 'assets/boom.png';
      } else if (widget.pandon == widget.currentMoney) {
        tempImgSrc = 'assets/onecoin.png';
      } else {
        tempImgSrc = 'assets/twocoin.png';
      }
    } else {
      tempImgSrc = 'assets/cup.png';
    }

    setState(() {
      imgSrc = tempImgSrc;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _calculateStart();
        _calculateImgSrc();
      },
      child: Image.asset(
        imgSrc,
        width: 150,
        height: 150,
        fit: BoxFit.fill,
      ),
    );
  }
} 